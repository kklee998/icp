#include <stdio.h>

float calculate_sales(float commission);

void main()
{
	int sales;
	float salary;

	printf("Enter Sales: ");
	scanf("%d", &sales);
	printf("\nSalary is RM: %.2f\n", calculate_sales(sales)+100);
}

float calculate_sales (float commission)
{
	float result;
	result = commission * 0.15;

	return result;
}