/* Write an array program that computes the class test average score.
 *
 * Instructions:
 *
 * prompt user to input the test marks
 *
 * compute the sum of the array ’testarray’
 *
 * display the average mark
 *
 * ** the class test marks (less than 10) */

#include <stdio.h>

int main()
{
    float testarray[10], result = 0;
    int i,k;

    printf("Please enter the scores of 10 students: \n");
    for (i = 0; i < 10; i++) {
       
        scanf("%f", &testarray[i]);
    }
   
    for (k = 0; k < 10; k++){
        
        result = result + testarray[k];
    }
    printf("The average mark of 10 students is: %.2f\n", result/10);

    return 0;
}


