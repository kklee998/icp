#include <stdio.h>
int reverseDigit(int);

void main()
{
    int x;
    printf("Enter a number between 1 and 9999: ");
    scanf("%d", &x);

    printf("\nThe number with its digits reversed: %d\n", reverseDigit(x));
}

int reverseDigit(int y)
{
    int result = 0, remainder;
    while (y != 0){
        remainder = y % 10;
        result = result * 10 + remainder;
        y = y / 10;
    }

    return result;
}
