#include <stdio.h>

int main()
{
    int count_star = 0, n = 10;
    while (count_star < n) {
        printf("%d*\n", count_star);
        count_star++;
    }

    return 0;
}
