#include <stdio.h>

int recur(int num);

void main()
{
	int input;

	printf("Enter an integer: ");
	scanf("%d", &input);
	printf("\n%d! = %d\n", input, recur(input));
}

int recur (int num)
{
	if (num <=1) {
		return 1;
	}
	return num * recur (num - 1);
}	