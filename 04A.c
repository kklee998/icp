/* PROGRAM TO CALCULATE PRICE OF ITEM AFTER TAX */

#include <stdio.h>

int main()
{
    float ItemPrice = 0, cityStateSaleTax = 0.055, luxuryTax = 0.1, amountDue = 0;

    printf("Please enter item price: \n");
    scanf("%f",&ItemPrice);

    if (ItemPrice < 50000)
    	amountDue = ItemPrice * (1 + cityStateSaleTax);
    else
    	amountDue = ItemPrice * (1 + cityStateSaleTax + luxuryTax);

    printf("The price of the item is: %f \n", amountDue );

    return 0; 
}

