#include<stdio.h>

#define PI 3.14

int main()
{
   int radius;
   char choice;

   printf("Enter the value of radius (cm): \n");
   scanf("%d", &radius);

   printf("Calculate\n"
           "(A)rea\n"
           "(C)ircumference\n"
           "(D)iameter\n");
    
   scanf(" %c", &choice);
   switch(choice)
   {
       case 'A':
           printf("Option: A\n"
                   "The area of circle in cm is: %.2f\n", (radius*radius)*PI);
           break;
       case 'C':
           printf("Option: C\n"
                   "The circumference of the circle in cm is: %.2f\n", (2*PI*radius));
           break;
       case 'D':
            printf("Option: D\n"
                    "The diameter of the circle in cm is: %d\n", (2*radius));
               break;
       default:
               printf("You have entered an invalid key.\n");
   }

   return 0;
}

