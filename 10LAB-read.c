//reading a file
#include<stdio.h>

int main()
{
    FILE *f;
    int data;
    f = fopen("outfile.txt","r");
    if (f == NULL)
        printf("Could not open file.\n");

    while (!feof(f))
    {
        fscanf(f,"%d\n",&data);
        printf("%d\n", data);
    }
    fclose(f);
    return 0;
}
