//writing number 1 to 10 into a file
#include<stdio.h>
int main()
{
    FILE *f;
    int x, MAX=10;
    f = fopen("outfile.txt","w");
    if (f==NULL)
        printf("Could not create\n");
    for(x = 1; x <=MAX; x++)
        fprintf(f,"%d\n",x);
    fclose(f);
    return 0;
}
