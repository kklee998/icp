#include <stdio.h>
#include <stdlib.h>
struct node
{
	int data;
	struct node *next;
	//self-referencing pointer
};

void main()
{
	struct node *list, *head, *newnode;
	newnode = (struct node *)malloc(sizeof(struct node));
	newnode->data = 6;
	newnode->next = NULL;
	list = newnode;
	head = list;
	//Display the content of the list
	list = head;
	while(list!=NULL)
	{
		printf("\n %d", list->data);
		list = list->next;
	} //end while
	printf("\n=============================\n");
	
	//Add one element
	list = head;
	while(list->next != NULL)
	{
		list = list->next;
	} //end while
	newnode = (struct node *)malloc(sizeof(struct node));
	newnode->data = 12;
	newnode->next = NULL;
	list->next = newnode;
	head = list;
	
	//Display both elements
	while(list!=NULL)
	{
		printf("\n %d", list->data);
		list = list->next;
	} //end while
}